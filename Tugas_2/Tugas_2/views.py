from django.http import HttpResponse
from django.shortcuts import render
from datetime import datetime, date

mhs_name = "Elvin Nur Furqon"
curr_year = int(datetime.now().strftime("%Y"))
npm = None


def index(request):
    response = {'name': mhs_name}
    return render(request, 'profil.html', response)
